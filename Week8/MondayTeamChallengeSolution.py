def get_input():
    boolean = input("1) Enter true or True\n2) Enter false or False\n3) Enter exit to quit \n")
    while str_to_bool(boolean) == "bad":
       boolean = input("Bad input\n\n1) Enter true or True\n2) Enter false or False\n3) Enter exit to quit \n")
    boolean = str_to_bool(boolean)
    return boolean

def reverse(boolean):
    if boolean == True:
        rv = False
    elif boolean == False:
        rv = True
    else:
        print("Not a valid boolean")
    return rv

def str_to_bool(string):
    if string.lower() == "true":
        rv = True
    elif string.lower() == "false":
        rv = False
    elif string.lower() == "exit":
        rv = "exit"
    else:
        rv = "bad"
    return rv

def display(boolean, opposite):
    print(f"\nthe reverse of {boolean} is {opposite} \n")

def main():
    print("Welcome to swapabool, the boolean swapper.  This program will swap\nyour boolean value for its opposite, pretty cool right?!?!?!\n\n")
    while True:
        boolean = get_input()
        if type(boolean) is not bool and boolean.lower() == "exit":
           print("goodbye!!")
           break
        opposite = reverse(boolean)
        display(boolean, opposite)
main()
