from os.path import isfile

def get_tags(ifilename):
  tags = []
  if isfile(ifilename):
    try:
      with open(ifilename, "r+") as file:
        for line in file:
          if "<" in line and ">" in line:
            tag = line[line.find("<")+1:line.find(">")]
            if tag not in tags and tag[0] != "/":
              tags.append(tag)
    except:
      print("nonspecific read error occurred")
    return tags
  else:
    print(f"input file does not exist: {ifilename}")
    return False

def save(tags, ofilename, ifilename):
  with open(ofilename, "a") as file:
    file.write(f"\n\n{ifilename} tags:\n\n")
    for tag in tags:
      file.write(f"<{tag}>...</{tag}>\n")

def main():
  ifiles = ["otherstuff.xml", "stuff.xml"]
  for f in ifiles:
    ifilename = f
    ofilename = "tags.dat"
    tags = get_tags(ifilename)
    if tags:
      save(tags, ofilename, ifilename)
      print(f"Tags read and saved to {ofilename}, {len(tags)} unique tags in file {ifilename}")

main()
