def input_data(mini, maxi, prompt):
    print(f"{prompt}: ")
    while True:
      try:
        inp = input()
        inp = float(inp)
        while inp < mini or inp > maxi:
          print(f"Invalid entry, {inp} is less than {mini} or greater than {maxi}")
          inp = float(input(f"{prompt}: "))
        break
      except:
        print(f"invalid input {inp}, try again")
    return inp

def outputData(grossPay):
    print(f"The gross pay for this worker is ${grossPay:.2f}")

def save_data(name, grossPay, hours, rate, ofilename):
    with open(ofilename, "a") as file:
      file.write(f"{name:30}:::{grossPay:10}:::{hours:5}:::{rate:6}\n")

def processData(rate, hours):
    grossPay = rate * hours
    return grossPay

def main():
  hours = input_data(0, 40, "Enter the number of hours worked")
  rate = input_data(0, 1000, "Enter the rate of pay")
  name = input("Enter a name to be associated with this pay calculation")
  gross = processData(rate, hours)
  save_data(name, gross, hours, rate, "report.dat")
  outputData(gross)

main()
